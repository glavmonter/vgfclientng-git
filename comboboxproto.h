#ifndef COMBOBOXPROTO_H
#define COMBOBOXPROTO_H

#include <QObject>
#include <QComboBox>
#include <QMap>
#include "VGFThermo.pb.h"

enum ComboBoxProtoType {
    TYPE_NORMAL = 0,
    TYPE_GROUP
};

class ComboBoxProto : public QComboBox
{
    Q_OBJECT

public:
    explicit ComboBoxProto(QWidget *parent = 0);
    ~ComboBoxProto();

    void setType(ComboBoxProtoType type);

    Thermo::RegulatorMode getMode();

public slots:
    void setCurrentIndexProto(Thermo::RegulatorMode mode);

private:
    ComboBoxProtoType m_xType;

    QMap<Thermo::RegulatorMode, int> m_mModeToIndex;
    QMap<int, Thermo::RegulatorMode> m_mIndexToMode;
};

#endif // COMBOBOXPROTO_H
