
#include "zonetempplot.h"

#include <qwt_plot_grid.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_directpainter.h>
#include <qwt_curve_fitter.h>
#include <qwt_painter.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_panner.h>

#include <QEvent>

class CurveData: public QwtArraySeriesData<QPointF>
{
public:
    CurveData()
    {
    }

    virtual QRectF boundingRect() const
    {
        if (d_boundingRect.width() < 0.0)
            d_boundingRect = qwtBoundingRect(*this);

        return d_boundingRect;
    }

    inline void append(const QPointF &point)
    {
        d_samples += point;
    }

    void clear()
    {
        d_samples.clear();
        d_samples.squeeze();
        d_boundingRect = QRectF(0.0, 0.0, -1.0, -1.0);
    }

    void clearStaleVal(double limit)
    {
        const QVector<QPointF> values = d_samples;
        d_samples.clear();

        int index;
        for (index = values.size() - 1; index >= 0; index--)
        {
            if (values[index].x() < limit)
                break;
        }

        if (index > 0)
            d_samples += (values[index++]);

        while (index < values.size() - 1)
            d_samples += (values[index++]);

        d_boundingRect = QRectF(0.0, 0.0, -1.0, -1.0);
    }
};

ZoneTempPlot::ZoneTempPlot(QWidget *parent):
    QwtPlot(parent),
    paintedPointsCurrentT(0),
    d_interval(0.0, 3.0),
    d_timerId(-1)
{
    painterCurrentT = new QwtPlotDirectPainter();

    setAutoReplot(false);

    initGradient();

    plotLayout()->setAlignCanvasToScales(true);

    setAxisTitle(QwtPlot::xBottom, "Time [h]");
    setAxisScale(QwtPlot::xBottom, d_interval.minValue(), d_interval.maxValue()); 
    setAxisScale(QwtPlot::yLeft, 0, 1000.0);

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(QPen(Qt::green, 0.0, Qt::DotLine));
    grid->enableX(true);
    grid->enableXMin(true);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(this);

    curveCurrentT = new QwtPlotCurve();
    curveCurrentT->setStyle(QwtPlotCurve::Lines);
    curveCurrentT->setPen(QPen(Qt::green, 3));
    curveCurrentT->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    curveCurrentT->setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
    curveCurrentT->setData(new CurveData());
    curveCurrentT->attach(this);

    QwtPlotMagnifier *magnifier = new QwtPlotMagnifier(this->canvas());
    magnifier->setMouseButton(Qt::MidButton);

    QwtPlotPanner *panner = new QwtPlotPanner(this->canvas());
    panner->setMouseButton(Qt::RightButton);
}

ZoneTempPlot::~ZoneTempPlot()
{
    delete painterCurrentT;
}

void ZoneTempPlot::initGradient()
{
    QPalette pal = canvas()->palette();

#if QT_VERSION >= 0x040400
    QLinearGradient gradient(0.0, 0.0, 1.0, 0.0);
    gradient.setCoordinateMode(QGradient::StretchToDeviceMode);
    gradient.setColorAt(0.0, QColor(0, 49, 110));
    gradient.setColorAt(1.0, QColor(0, 87, 174));

    pal.setBrush(QPalette::Window, QBrush(gradient));
#else
    pal.setBrush(QPalette::Window, QBrush(color));
#endif

    canvas()->setPalette(pal);
}

void ZoneTempPlot::start(bool started)
{
    if (started) {
        d_clock.start();
        d_timerId = startTimer(500);

    } else {
        killTimer(d_timerId);
    }
}

void ZoneTempPlot::replot()
{
    CurveData *data0 = static_cast<CurveData *>(curveCurrentT->data());
    QwtPlot::replot();
    paintedPointsCurrentT = data0->size();
}

void ZoneTempPlot::setIntervalLength(double interval)
{
    if (interval > 0.0 && interval != d_interval.width())
    {
        d_interval.setMaxValue(d_interval.minValue() + interval);
        setAxisScale(QwtPlot::xBottom, 
            d_interval.minValue(), d_interval.maxValue());

        replot();
    }
}

void ZoneTempPlot::updateCurve()
{
    CurveData *data0 = (CurveData *)curveCurrentT->data();
    const int numPoints0 = data0->size();

    if ((numPoints0 > paintedPointsCurrentT))
    {
        const bool doClip = !canvas()->testAttribute(Qt::WA_PaintOnScreen);
        if (doClip)
        {
            /*
                Depending on the platform setting a clip might be an important
                performance issue. F.e. for Qt Embedded this reduces the
                part of the backing store that has to be copied out - maybe
                to an unaccelerated frame buffer device.
            */

            const QwtScaleMap xMap0 = canvasMap(curveCurrentT->xAxis());
            const QwtScaleMap yMap0 = canvasMap(curveCurrentT->yAxis());

            QRectF br0 = qwtBoundingRect(*data0,
                paintedPointsCurrentT - 1, numPoints0 - 1);

            const QRect clipRect0 = QwtScaleMap::transform(xMap0, yMap0, br0).toRect();
            painterCurrentT->setClipRegion(clipRect0);
        }

        painterCurrentT->drawSeries(curveCurrentT,
            paintedPointsCurrentT - 1, numPoints0 - 1);
        paintedPointsCurrentT = numPoints0;
    }
}

void ZoneTempPlot::appendPoint0(double u)
{
    double elapsed = d_clock.elapsed() / 1000.0 / 3600.0; // ms->s->h
//    qDebug() << "d_clock0: " << elapsed;
//    qDebug() << "udouble0: " << u;
    QPointF point(elapsed, u);
    CurveData *data = static_cast<CurveData *>(curveCurrentT->data());
    data->append(point);

    const int numPoints = data->size();
    if (numPoints > paintedPointsCurrentT)
    {
        const bool doClip = !canvas()->testAttribute(Qt::WA_PaintOnScreen);
        if (doClip)
        {
            /*
                Depending on the platform setting a clip might be an important
                performance issue. F.e. for Qt Embedded this reduces the
                part of the backing store that has to be copied out - maybe
                to an unaccelerated frame buffer device.
            */

            const QwtScaleMap xMap = canvasMap(curveCurrentT->xAxis());
            const QwtScaleMap yMap = canvasMap(curveCurrentT->yAxis());

            QRectF br = qwtBoundingRect(*data,
                paintedPointsCurrentT - 1, numPoints - 1);

            const QRect clipRect = QwtScaleMap::transform(xMap, yMap, br).toRect();
            painterCurrentT->setClipRegion(clipRect);
        }

        painterCurrentT->drawSeries(curveCurrentT,
            paintedPointsCurrentT - 1, numPoints - 1);
        paintedPointsCurrentT = numPoints;
    }
}

void ZoneTempPlot::appendCurrentT(double ts, double T)
{
    qDebug() << "Append current temperature at time: " << ts <<  " = " << T;
}

void ZoneTempPlot::appendPresetT(double ts, double T)
{
    qDebug() << "Append preset temperature at time: " << ts << " = " << T;
}


void ZoneTempPlot::incrementInterval()
{
    d_interval = QwtInterval(d_interval.maxValue(),
        d_interval.maxValue() + d_interval.width());

    CurveData *data0 = static_cast<CurveData *>(curveCurrentT->data());
    data0->clearStaleVal(d_interval.minValue());

    QwtScaleDiv scaleDiv = axisScaleDiv(QwtPlot::xBottom);
    scaleDiv.setInterval(d_interval);

    for (int i = 0; i < QwtScaleDiv::NTickTypes; i++)
    {
        QList<double> ticks = scaleDiv.ticks(i);
        for (int j = 0; j < ticks.size(); j++)
            ticks[j] += d_interval.width();
        scaleDiv.setTicks(i, ticks);
    }
    setAxisScaleDiv(QwtPlot::xBottom, scaleDiv);

    paintedPointsCurrentT = 0;
    replot();
}

void ZoneTempPlot::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == d_timerId)
    {
        updateCurve();

        const double elapsed = d_clock.elapsed() / 1000.0 / 3600.0;
        if (elapsed > d_interval.maxValue())
            incrementInterval();

        return;
    }

    QwtPlot::timerEvent(event);
}

void ZoneTempPlot::resizeEvent(QResizeEvent *event)
{
    painterCurrentT->reset();
    QwtPlot::resizeEvent(event);
}
