#include "framezone.h"
#include "ui_framezone.h"


FrameZone::FrameZone(int number, QWidget *parent) :
    QFrame(parent),
    ui(new Ui::FrameZone)
{
    ui->setupUi(this);
    m_iNumber = number;
    ui->labelNumber->setText(tr("%1").arg(m_iNumber + 1));
    QObject::connect(ui->actionShowSettings, SIGNAL(triggered()), this, SIGNAL(showSettings()));
}

FrameZone::~FrameZone()
{
    delete ui;
}

void FrameZone::UpdateRoFields()
{
//    ui->labelCurrentT->setText(tr("%1").arg(DataDriver::getInstance().getCurrentT(m_iNumber), 0, 'f', 1));
}

void FrameZone::on_ledStatus_customContextMenuRequested(const QPoint &pos)
{
    QMenu *menu = new QMenu(this);

    menu->addAction(ui->actionShowSettings);
    menu->exec(ui->ledStatus->mapToGlobal(pos));
}

void FrameZone::ContextMenuRequested(const QPoint &pos)
{
    QMenu *menu = new QMenu(this);
    ui->actionShowSettings->setText(tr("Settings %1").arg(m_iNumber + 1));
    menu->addAction(ui->actionShowSettings);
    menu->exec(this->mapToGlobal(pos));
}

void FrameZone::updateColor()
{
bool run = false;
bool err = false;
    run =   (m_xRegulatorMode == Thermo::MODE_MANUAL) ||
            (m_xRegulatorMode == Thermo::MODE_RELAY) ||
            (m_xRegulatorMode == Thermo::MODE_PID);

    err =   (m_xSendiag != Thermo::DeviceToHost_SendiagFlags_SENDIAG_NORMAL);


    if (!run && !err) {
        ui->ledStatus->setValue(false);
        ui->ledStatus->setToolTip(tr("Stopped. Closed thermocouple"));
    }

    if (!run && err) {
        ui->ledStatus->setOnColor(QLed::Yellow);
        ui->ledStatus->setValue(true);
        ui->ledStatus->setToolTip(tr("Stopped. Open thermocouple"));
    }

    if (run && !err) {
        ui->ledStatus->setOnColor(QLed::Green);
        ui->ledStatus->setValue(true);
        ui->ledStatus->setToolTip(tr("Running. Closed thermocouple"));
    }

    if (run && err) {
        ui->ledStatus->setOnColor(QLed::Red);
        ui->ledStatus->setValue(true);
        ui->ledStatus->setToolTip(tr("Running. Open thermocouple"));
    }
}

void FrameZone::setCurrentT(int zoneIndex, double T)
{
    if (zoneIndex == m_iNumber) {
        ui->labelCurrentT->setText(QString::number(T, 'f', 1));
    }
}

void FrameZone::setSendiag(int zoneIndex, quint32 sendiag)
{
    if (zoneIndex != m_iNumber)
        return;

    m_xSendiag = static_cast<Thermo::DeviceToHost_SendiagFlags>(sendiag);
    updateColor();
}

void FrameZone::setRegulatorMode(int zoneIndex, Thermo::RegulatorMode mode)
{
    if (zoneIndex != m_iNumber)
        return;
    m_xRegulatorMode = mode;
    updateColor();
}
