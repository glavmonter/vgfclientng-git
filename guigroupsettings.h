#ifndef GUIGROUPSETTINGS_H
#define GUIGROUPSETTINGS_H

#include <QWidget>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QKeyEvent>
#include "comboboxproto.h"

#include "protocol_tcp.pb.h"
#include "VGFThermo.pb.h"

namespace Ui {
class GuiGroupSettings;
}

class GuiGroupSettings : public QWidget
{
    Q_OBJECT

public:
    explicit GuiGroupSettings(QWidget *parent = 0);
    ~GuiGroupSettings();

    bool getHostToDevice(Thermo::Payload *msg);
    void keyPressEvent(QKeyEvent *e);


private:
    Ui::GuiGroupSettings *ui;

    QCheckBox *m_pCheck_1;
    QCheckBox *m_pCheck_2;
    QCheckBox *m_pCheck_3;

    QCheckBox *m_pCheckAll;
    QList<QCheckBox *> m_lZonesChecked;
    QList<QLineEdit *> m_lLineEdit;

private slots:
    void discard();
    void reset();
    void localAccepted();


signals:
    void accepted();
};

#endif // GUIGROUPSETTINGS_H
