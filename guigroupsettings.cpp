#include <QDebug>
#include <QLocale>
#include "guigroupsettings.h"
#include "ui_guigroupsettings.h"

GuiGroupSettings::GuiGroupSettings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GuiGroupSettings)
{
    ui->setupUi(this);

    ui->cmbMode->setType(ComboBoxProtoType::TYPE_GROUP);

    ui->editProportional->setValidator(new QDoubleValidator(-100000.0, 100000.0, 12, this));
    ui->editIntegral->setValidator(new QDoubleValidator(-100000.0, 100000.0, 12, this));
    ui->editDerivative->setValidator(new QDoubleValidator(-100000.0, 100000.0, 12, this));
    ui->editPresetT->setValidator(new QDoubleValidator(-10, 1300, 1, this));
    ui->editPwmMax->setValidator(new QIntValidator(0, 65535, this));
    ui->editPwmOut->setValidator(new QIntValidator(0, 65535, this));

    m_lLineEdit << ui->editProportional;
    m_lLineEdit << ui->editIntegral;
    m_lLineEdit << ui->editDerivative;
    m_lLineEdit << ui->editPresetT;
    m_lLineEdit << ui->editPwmMax;
    m_lLineEdit << ui->editPwmOut;

    m_pCheck_1 = new QCheckBox(tr("1-8"), ui->groupZones);
    m_pCheck_2 = new QCheckBox(tr("9-16"), ui->groupZones);
    m_pCheck_3 = new QCheckBox(tr("17-24"), ui->groupZones);

    m_pCheckAll = new QCheckBox(tr("All"), ui->groupZones);
    QGridLayout *gridZones = new QGridLayout(ui->groupZones);

    gridZones->addWidget(m_pCheck_1, 9, 0, 1, 1, Qt::AlignLeft);
    gridZones->addWidget(m_pCheck_2, 9, 1, 1, 1, Qt::AlignLeft);
    gridZones->addWidget(m_pCheck_3, 9, 2, 1, 1, Qt::AlignLeft);
    gridZones->addWidget(m_pCheckAll, 10, 0, 1, 1, Qt::AlignLeft);

    connect(m_pCheckAll, SIGNAL(toggled(bool)), m_pCheck_1, SLOT(setChecked(bool)));
    connect(m_pCheckAll, SIGNAL(toggled(bool)), m_pCheck_2, SLOT(setChecked(bool)));
    connect(m_pCheckAll, SIGNAL(toggled(bool)), m_pCheck_3, SLOT(setChecked(bool)));

    for (int i = 0; i < 8; i++) {
        QCheckBox *cb = new QCheckBox(tr("Zone %1").arg(i + 1), ui->groupZones);
        connect(m_pCheckAll, SIGNAL(toggled(bool)), cb, SLOT(setChecked(bool)));
        connect(m_pCheck_1, SIGNAL(toggled(bool)), cb, SLOT(setChecked(bool)));

        gridZones->addWidget(cb, i, 0, 1, 1);
        m_lZonesChecked << cb;
    }

    for (int i = 0; i < 8; i++) {
        QCheckBox *cb = new QCheckBox(tr("Zone %1").arg(i + 9), ui->groupZones);
        connect(m_pCheckAll, SIGNAL(toggled(bool)), cb, SLOT(setChecked(bool)));
        connect(m_pCheck_2, SIGNAL(toggled(bool)), cb, SLOT(setChecked(bool)));

        gridZones->addWidget(cb, i, 1, 1, 1);
        m_lZonesChecked << cb;
    }

    for (int i = 0; i < 8; i++) {
        QCheckBox *cb = new QCheckBox(tr("Zone %1").arg(i + 17), ui->groupZones);
        connect(m_pCheckAll, SIGNAL(toggled(bool)), cb, SLOT(setChecked(bool)));
        connect(m_pCheck_3, SIGNAL(toggled(bool)), cb, SLOT(setChecked(bool)));

        gridZones->addWidget(cb, i, 2, 1, 1);
        m_lZonesChecked << cb;
    }

    connect(reinterpret_cast<QPushButton *>(ui->buttonBox->button(QDialogButtonBox::Ok)), SIGNAL(clicked()), this, SIGNAL(accepted()));
    connect(reinterpret_cast<QPushButton *>(ui->buttonBox->button(QDialogButtonBox::Ok)), SIGNAL(clicked()), this, SLOT(reset()));
    connect(reinterpret_cast<QPushButton *>(ui->buttonBox->button(QDialogButtonBox::Ok)), SIGNAL(clicked()), this, SLOT(close()));

    connect(reinterpret_cast<QPushButton *>(ui->buttonBox->button(QDialogButtonBox::Cancel)), SIGNAL(clicked()), this, SLOT(close()));
    connect(reinterpret_cast<QPushButton *>(ui->buttonBox->button(QDialogButtonBox::Cancel)), SIGNAL(clicked()), this, SLOT(reset()));
    connect(reinterpret_cast<QPushButton *>(ui->buttonBox->button(QDialogButtonBox::Reset)), SIGNAL(clicked()), this, SLOT(reset()));

    connect(reinterpret_cast<QPushButton *>(ui->buttonBox->button(QDialogButtonBox::Apply)), SIGNAL(clicked()), this, SIGNAL(accepted()));
}

GuiGroupSettings::~GuiGroupSettings()
{
    delete ui;
}


bool GuiGroupSettings::getHostToDevice(Thermo::Payload *msg)
{
bool need_return = false;

bool is_not_empty = !ui->editProportional->text().isEmpty() ||
                        !ui->editIntegral->text().isEmpty() ||
                        !ui->editDerivative->text().isEmpty() ||
                        !ui->editPresetT->text().isEmpty() ||
                        !ui->editPwmMax->text().isEmpty() ||
                        !ui->editPwmOut->text().isEmpty() ||
                        ui->chkResetPid->isChecked() ||
                        (ui->cmbMode->currentIndex() != 0);

    if (!is_not_empty)
    return false;

    for (int zone_number = 0; zone_number < m_lZonesChecked.size(); zone_number++) {
        if (m_lZonesChecked[zone_number]->isChecked()) {
            need_return = true;
            Thermo::HostToDevice *htd = msg->add_htd();
            htd->set_zonenumber(zone_number);

            QLocale loc = QLocale::system();

            if (!ui->editProportional->text().isEmpty())
                htd->set_kp(loc.toFloat(ui->editProportional->text()));

            if (!ui->editIntegral->text().isEmpty())
                htd->set_ki(loc.toFloat(ui->editIntegral->text()));

            if (!ui->editDerivative->text().isEmpty())
                htd->set_kd(loc.toFloat(ui->editDerivative->text()));

            if (!ui->editPresetT->text().isEmpty())
                htd->set_presett(loc.toFloat(ui->editPresetT->text()));

            if (!ui->editPwmMax->text().isEmpty())
                htd->set_pwmmaximum(ui->editPwmMax->text().toInt());

            if (!ui->editPwmOut->text().isEmpty())
                htd->set_pwmout(ui->editPwmOut->text().toInt());

            if (ui->chkResetPid->isChecked())
                htd->set_resetpid(true);

            if (ui->cmbMode->currentIndex() != 0) {
                htd->set_mode(ui->cmbMode->getMode());
            }
        }
    }
    return need_return;
}

void GuiGroupSettings::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape) {
        close();
    } else {
        QWidget::keyPressEvent(e);
    }
}

void GuiGroupSettings::discard()
{
}

void GuiGroupSettings::reset()
{
    foreach (QLineEdit *edit, m_lLineEdit) {
        edit->clear();
    }
    foreach (QCheckBox *cb, m_lZonesChecked) {
        cb->setChecked(false);
    }

    ui->chkResetPid->setChecked(false);
    m_pCheck_1->setChecked(false);
    m_pCheck_2->setChecked(false);
    m_pCheck_3->setChecked(false);
    m_pCheckAll->setChecked(false);
    ui->cmbMode->setCurrentIndex(0);
}

void GuiGroupSettings::localAccepted()
{
    emit accepted();
}
