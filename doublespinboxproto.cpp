#include "doublespinboxproto.h"

DoubleSpinBoxProto::DoubleSpinBoxProto(QWidget *parent) :
    QDoubleSpinBox(parent)
{
    m_dLastData = value();
    connect(this, SIGNAL(valueChanged(double)), SLOT(onValueChanged(double)));
}

DoubleSpinBoxProto::~DoubleSpinBoxProto()
{
}

void DoubleSpinBoxProto::keyPressEvent(QKeyEvent *event)
{
    if ((event->modifiers() == Qt::ControlModifier) && (event->key() == Qt::Key_Z))
        return;
    QDoubleSpinBox::keyPressEvent(event);
}

void DoubleSpinBoxProto::setChanged(bool ch)
{
QPalette pal(palette());
QColor colorWhite(255, 255, 255);
QColor colorYellow(255, 255, 0);

    if (ch) {
        pal.setColor(QPalette::Base, colorYellow);
        setToolTip(tr("%1").arg(m_dLastData, 0, 'g', 12));
    } else {
        pal.setColor(QPalette::Base, colorWhite);
        setToolTip(tr(""));
    }
    setPalette(pal);
}

void DoubleSpinBoxProto::onValueChanged(double val)
{
    m_bChanged = (qAbs(m_dLastData - val) > 1e-13) ? true : false;
    setChanged(m_bChanged);
}

void DoubleSpinBoxProto::apply()
{
    m_dLastData = value();
    m_bChanged = false;
    setChanged(false);
}

void DoubleSpinBoxProto::discard()
{
    if (qAbs(m_dLastData - value() > 1e-12))
        return;

    setValue(m_dLastData);
    m_bChanged = false;
    setChanged(false);
}

bool DoubleSpinBoxProto::getData(google::protobuf::Message *msg)
{
    if (!m_bChanged)
        return false;

const google::protobuf::Descriptor *desc = msg->GetDescriptor();
const google::protobuf::Reflection *refl = msg->GetReflection();
const google::protobuf::FieldDescriptor *field = desc->FindFieldByName(m_sFieldName.toStdString());
    assert(field != NULL);

    refl->SetFloat(msg, field, value());

    apply();
    return true;
}

QString DoubleSpinBoxProto::fieldName() const
{
    return m_sFieldName;
}

void DoubleSpinBoxProto::setFieldName(const QString &str)
{
    m_sFieldName = str;
}
