#ifndef DOUBLESPINBOXPROTO_H
#define DOUBLESPINBOXPROTO_H

#include <QObject>
#include <QDoubleSpinBox>
#include <QKeyEvent>
#include <QPalette>
#include <google/protobuf/message.h>

class DoubleSpinBoxProto : public QDoubleSpinBox
{
    Q_OBJECT
    Q_PROPERTY(QString fieldName READ fieldName WRITE setFieldName)

public:
    explicit DoubleSpinBoxProto(QWidget *parent = 0);
    ~DoubleSpinBoxProto();

private:
    void keyPressEvent(QKeyEvent *event);
    void setChanged(bool ch);

    double m_dLastData;
    bool m_bChanged;

    QString m_sFieldName;

private slots:
    void onValueChanged(double val);


public slots:
    void apply();
    void discard();


public:
    bool getData(google::protobuf::Message *msg);

    QString fieldName() const;
    void setFieldName(const QString &str);
};

#endif // DOUBLESPINBOXPROTO_H
