#ifndef THERMOCOUPLECONVERTER_H
#define THERMOCOUPLECONVERTER_H

#include <QVector>

class ThermocoupleTypeK
{
public:
    ThermocoupleTypeK();
    ~ThermocoupleTypeK();

    double GetTemperature(double Emf_uV);
    double GetEmf(double T);

private:
    QVector<double> TtoEmf;
    QVector<double> EmfToT;

    double a0 = 0.118597600000E+00;
    double a1 = -0.118343200000E-03;
    double a2 = 0.126968600000E+03;
};


class ThermocoupleTypeS
{
public:
    ThermocoupleTypeS();
    ~ThermocoupleTypeS();

    double GetTemperature(double Emf_uV);
    double GetEmf(double T);

private:
    double pGetTemperature(double Emf, QVector<double> &poly);

    QVector<double> EmfToT0;
    QVector<double> EmfToT1;
    QVector<double> EmfToT2;
    QVector<double> EmfToT3;

    QVector<double> TtoEmf;
};

#endif // THERMOCOUPLECONVERTER_H
