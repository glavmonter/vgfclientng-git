#-------------------------------------------------
#
# Project created by QtCreator 2014-03-07T12:01:18
#
#-------------------------------------------------

QT       += core gui network svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VGFClientNg
TEMPLATE = app
CONFIG += debug_and_release c++11

SOURCES +=  main.cpp \
            mainwindow.cpp \
            plotfurnace.cpp \
            zonetempplot.cpp \
            qled.cpp \
            doublespinboxproto.cpp \
            intspinboxproto.cpp \
            framezone.cpp \
            guizonesettings.cpp \
            guigroupsettings.cpp \
            modelsummary.cpp \
            guisummary.cpp \
            comboboxproto.cpp \
            customplotpicker.cpp \
            thermocoupleconverter.cpp

HEADERS  += mainwindow.h \
            qled.h \
            doublespinboxproto.h \
            intspinboxproto.h \
            plotfurnace.h \
            zonetempplot.h \
            framezone.h \
            guizonesettings.h \
            guigroupsettings.h \
            modelsummary.h \
            guisummary.h \
            comboboxproto.h \
            customplotpicker.h \
            thermocoupleconverter.h

FORMS    += mainwindow.ui\
            framezone.ui \
            guizonesettings.ui \
            guigroupsettings.ui \
            guisummary.ui


TRANSLATIONS += \
            lang/VGFClientNg_ru_RU.ts

PROTOS +=   ../protos/protocol_tcp.proto \
            ../protos/VGFThermo.proto \
            ../protos/VGFPlatinum.proto

include (protobuf.pri)

unix {
    LIBS += -lprotobuf 

    INCLUDEPATH += /usr/include/qwt6-qt5/
    LIBS += -lqwt-qt5
}

win32 {
}

RESOURCES += \
            vgfclientng.qrc

OTHER_FILES += \
            controller.proto
