#include <math.h>
#include "thermocoupleconverter.h"

ThermocoupleTypeK::ThermocoupleTypeK()
{
    EmfToT  <<  0.000000E+00 <<  2.508355E+01 <<  7.860106E-02
            << -2.503131E-01 <<  8.315270E-02 << -1.228034E-02
            <<  9.804036E-04 << -4.413030E-05 <<  1.057734E-06
            << -1.052755E-08;

    TtoEmf  << -0.176004136860E-01 <<  0.389212049750E-01 <<  0.185587700320E-04
            << -0.994575928740E-07 <<  0.318409457190E-09 << -0.560728448890E-12
            <<  0.560750590590E-15 << -0.320207200030E-18 <<  0.971511471520E-22
            << -0.121047212750E-25;
}

ThermocoupleTypeK::~ThermocoupleTypeK()
{}

double ThermocoupleTypeK::GetTemperature(double Emf_uV)
{
double mV = Emf_uV/1000.0;
double T = 0.0;

    for (int i = 0; i < EmfToT.size(); i++)
        T += EmfToT[i]*pow(mV, i);
    return T;
}

double ThermocoupleTypeK::GetEmf(double T)
{
double emf = 0.0;

    for (int i = 0; i < TtoEmf.size(); i++)
        emf += TtoEmf[i]*pow(T, i);
    emf += a0*exp(a1*(T - a2)*(T - a2));
    return emf;
}



ThermocoupleTypeS::ThermocoupleTypeS()
{
    EmfToT0 <<  0.00000000E+00 <<  1.84949460E+02 << -8.00504062E+01
            <<  1.02237430E+02 << -1.52248592E+02 <<  1.88821343E+02
            << -1.59085941E+02 <<  8.23027880E+01 << -2.34181944E+01
            <<  2.79786260E+00;

    EmfToT1 <<  1.291507177E+01 <<  1.466298863E+02 << -1.534713402E+01
            <<  3.145945973E+00 << -4.163257839E-01 <<  3.187963771E-02
            << -1.291637500E-03 <<  2.183475087E-05 << -1.447379511E-07
            <<  8.211272125E-09;


    EmfToT2 << -8.087801117E+01 <<  1.621573104E+02 << -8.536869453E+00
            <<  4.719686976E-01 << -1.441693666E-02 <<  2.081618890E-04;


    EmfToT3 <<  5.333875126E+04 << -1.235892298E+04 << 1.092657613E+03
            << -4.265693686E+01 <<  6.247205420E-01;

    TtoEmf  <<  0.000000000000E+00 <<  0.540313308631E-02 <<  0.125934289740E-04
            << -0.232477968689E-07 <<  0.322028823036E-10 << -0.331465196389E-13
            <<  0.255744251786E-16 << -0.125068871393E-19 <<  0.271443176145E-23;
}

ThermocoupleTypeS::~ThermocoupleTypeS()
{}

double ThermocoupleTypeS::GetTemperature(double Emf_uV)
{
double Emf = Emf_uV/1000.0;
    if ((Emf > -0.235) && (Emf < 1.874))        // -50 to 250 C
        return pGetTemperature(Emf, EmfToT0);
    else if ((Emf > 1.874) && (Emf < 11.950))   //  250 to 1200 C
        return pGetTemperature(Emf, EmfToT1);
    else if ((Emf > 11.950) && (Emf < 17.536))  //  1064 to 1664.5
        return pGetTemperature(Emf, EmfToT2);

    return pGetTemperature(Emf, EmfToT3);
}

double ThermocoupleTypeS::GetEmf(double T)
{
double emf = 0.0;
    for (int i = 0; i < TtoEmf.size(); i++)
        emf += TtoEmf[i]*pow(T, i);
    return emf;
}


double ThermocoupleTypeS::pGetTemperature(double Emf, QVector<double> &poly)
{
double T = 0.0;

    for (int i = 0; i < poly.size(); i++)
        T += poly[i]*pow(Emf, i);
    return T;
}
