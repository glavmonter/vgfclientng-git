#ifndef CUSTOMPLOTPICKER_H
#define CUSTOMPLOTPICKER_H

#include <QObject>
#include <QDebug>
#include <qwt_plot_picker.h>

class CustomPlotPicker : public QwtPlotPicker
{
    Q_OBJECT

public:
    CustomPlotPicker(int xAxis, int yAxis,
        RubberBand rubberBand, DisplayMode trackerMode, QWidget * widget);
    ~CustomPlotPicker();


    QwtText trackerText(const QPoint &pos) const;

    void setPlotData(double *ZonePosition, double *CurrentT, double *PresetT, quint32 size);

private:
    double *m_pZonePosition = NULL;
    double *m_pCurrentT = NULL;
    double *m_pPresetT = NULL;

    quint32 m_iSize = 0;

};

#endif // CUSTOMPLOTPICKER_H
