#include "guisummary.h"
#include "ui_guisummary.h"

GuiSummary::GuiSummary(ModelSummary *model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GuiSummary)
{
    ui->setupUi(this);
    m_pModel = model;
    ui->tableView->setModel(m_pModel);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setStretchLastSection(true);

    connect(ui->buttonBox->button(QDialogButtonBox::Ok), &QPushButton::clicked, this, &GuiSummary::close);
}

GuiSummary::~GuiSummary()
{
    delete ui;
}

void GuiSummary::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape) {
        close();
    } else {
        QWidget::keyPressEvent(e);
    }
}

void GuiSummary::updateAll()
{
    ui->tableView->reset();
}
