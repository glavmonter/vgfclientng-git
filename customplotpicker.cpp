#include "customplotpicker.h"

CustomPlotPicker::CustomPlotPicker(int xAxis, int yAxis, QwtPicker::RubberBand rubberBand, QwtPicker::DisplayMode trackerMode, QWidget *widget)
    : QwtPlotPicker(xAxis, yAxis, rubberBand, trackerMode, widget)
{
}

CustomPlotPicker::~CustomPlotPicker()
{
}

QwtText CustomPlotPicker::trackerText(const QPoint &pos) const
{
QPointF plot_pos = invTransform(pos);

QString text;
int x_rounded = qRound(plot_pos.x());
qreal current_t = m_pCurrentT[x_rounded - 1];
qreal preset_t = m_pPresetT[x_rounded - 1];

    text.sprintf("%d: %.1f_%.1f", x_rounded, current_t, preset_t);
    return QwtText(text);
}

void CustomPlotPicker::setPlotData(double *ZonePosition, double *CurrentT, double *PresetT, quint32 size)
{
    m_pZonePosition = ZonePosition;
    m_pCurrentT = CurrentT;
    m_pPresetT = PresetT;
    m_iSize = size;
}

