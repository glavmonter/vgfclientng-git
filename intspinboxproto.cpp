#include "intspinboxproto.h"

IntSpinBoxProto::IntSpinBoxProto(QWidget *parent) :
    QSpinBox(parent)
{
    m_iLastData = 0;
    connect(this, SIGNAL(valueChanged(int)), SLOT(onValueChanged(int)));
}

IntSpinBoxProto::~IntSpinBoxProto()
{
}

void IntSpinBoxProto::keyPressEvent(QKeyEvent *event)
{
    if ((event->modifiers() == Qt::ControlModifier) && (event->key() == Qt::Key_Z))
        return;
    QSpinBox::keyPressEvent(event);
}

void IntSpinBoxProto::setChanged(bool ch)
{
QPalette pal(palette());

QColor colorWhite(255, 255, 255);
QColor colorYellow(255, 255, 0);

    if (ch) {
        pal.setColor(QPalette::Base, colorYellow);
        setToolTip(tr("Last value %1").arg(m_iLastData));
    } else {
        pal.setColor(QPalette::Base, colorWhite);
        setToolTip(tr(""));
    }
    setPalette(pal);
}

void IntSpinBoxProto::onValueChanged(int val)
{
    m_bChanged = (m_iLastData != val) ? true : false;
    setChanged(m_bChanged);
}

void IntSpinBoxProto::apply()
{
    m_iLastData = value();
    m_bChanged = false;
    setChanged(false);
}

void IntSpinBoxProto::discard()
{
    if (m_iLastData == value())
        return;

    setValue(m_iLastData);
    m_bChanged = false;
    setChanged(false);
}

bool IntSpinBoxProto::getData(google::protobuf::Message *msg)
{
    if (!m_bChanged)
        return false;

const google::protobuf::Descriptor *desc = msg->GetDescriptor();
const google::protobuf::Reflection *refl = msg->GetReflection();
const google::protobuf::FieldDescriptor *field = desc->FindFieldByName(m_sFieldName.toStdString());
    assert(field != NULL);

    refl->SetUInt32(msg, field, value());

    apply();
    return true;
}


QString IntSpinBoxProto::fieldName() const
{
    return m_sFieldName;
}

void IntSpinBoxProto::setFieldName(const QString &str)
{
    m_sFieldName = str;
}
