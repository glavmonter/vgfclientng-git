#include <iostream>

#include "guizonesettings.h"
#include "ui_guizonesettings.h"

GuiZoneSettings::GuiZoneSettings(int number, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GuiZoneSettings),
    m_bChanged(false),
    m_bNeedChangeMode(false)
{
    ui->setupUi(this);
    m_iNumber = number;

    setWindowTitle(tr("Zone %1").arg(m_iNumber + 1));
    ui->cmbMode->setType(ComboBoxProtoType::TYPE_NORMAL);

    ui->chkLockMode->setChecked(false);
    ui->chkLockPid->setChecked(false);
    ui->chkLockPWM->setChecked(false);

    ui->spinPresetT->apply();

    m_lIntSpinBoxes << ui->spinPwmMax << ui->spinPwmPreset;
    m_lDoubleSpinBoxes << ui->spinProportional << ui->spinIntegral << ui->spinDerivative;
    m_lDoubleSpinBoxes << ui->spinPresetT;

    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Ok)), SIGNAL(clicked()), this, SLOT(close()));

    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Cancel)), SIGNAL(clicked()), this, SLOT(close()));

    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Ok)), SIGNAL(clicked()), this, SLOT(myAccepted()));
    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Apply)), SIGNAL(clicked()), this, SLOT(myAccepted()));


    /* Отмена всех изменений */
    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Discard)), SIGNAL(clicked()), this, SLOT(discard()));
    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Discard)), SIGNAL(clicked()), ui->spinPwmMax, SLOT(discard()));
    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Discard)), SIGNAL(clicked()), ui->spinPwmPreset, SLOT(discard()));

    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Discard)), SIGNAL(clicked()), ui->spinProportional, SLOT(discard()));
    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Discard)), SIGNAL(clicked()), ui->spinIntegral, SLOT(discard()));
    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Discard)), SIGNAL(clicked()), ui->spinDerivative, SLOT(discard()));
    connect(reinterpret_cast<QObject *>(ui->buttonBox->button(QDialogButtonBox::Discard)), SIGNAL(clicked()), ui->spinPresetT, SLOT(discard()));

    connect(ui->cmbMode, SIGNAL(currentIndexChanged(int)), this, SLOT(slotModeChanged(int)));
    connect(ui->chkResetPid, SIGNAL(stateChanged(int)), this, SLOT(slotResetPidChanged(int)));

    ui->spinPwmMax->setFieldName("PwmMaximum");
    ui->spinPwmPreset->setFieldName("PwmOut");

    ui->spinProportional->setFieldName("Kp");
    ui->spinIntegral->setFieldName("Ki");
    ui->spinDerivative->setFieldName("Kd");
    ui->spinPresetT->setFieldName("PresetT");
}

GuiZoneSettings::~GuiZoneSettings()
{
    delete ui;
}

/**
 * @brief GuiZoneSettings::getHostToDevice
 * @param msg - Указатель на сообщение, которое будет передано на сервер
 * @return true - если найдены реальные изменения в диалогах
 */
bool GuiZoneSettings::getHostToDevice(Thermo::Payload *msg)
{
bool real_diff = false;
    if (m_bChanged) {
        Thermo::HostToDevice htd;
        htd.set_zonenumber(m_iNumber);
        qDebug() << tr("HostToDevice: %1 zone").arg(m_iNumber);

        foreach (IntSpinBoxProto *spin, m_lIntSpinBoxes) {
            if (spin->getData(&htd))
                real_diff = true;
        }

        foreach (DoubleSpinBoxProto *spin, m_lDoubleSpinBoxes) {
            if (spin->getData(&htd))
                real_diff = true;
        }

        if (ui->chkResetPid->isChecked()) {
            ui->chkResetPid->setChecked(false);
            real_diff = true;
            htd.set_resetpid(true);
        }

        if (m_bNeedChangeMode) {
            m_bNeedChangeMode = false;
            qDebug() << tr("Need change Mode");

            real_diff = true;
            htd.set_mode(ui->cmbMode->getMode());
        }

        if (real_diff) {
            std::cout << "Diffs found" << std::endl;
            std::cout << htd.DebugString() << std::endl;
            msg->add_htd()->MergeFrom(htd);
        }
    }

    m_bChanged = false;
    return real_diff;
}

void GuiZoneSettings::setInitialParameters(TCP::HostToDevice *init)
{
    blockSignals(true);
    ui->spinProportional->setValue(init->kp());
    ui->spinProportional->apply();

    ui->spinIntegral->setValue(init->ki());
    ui->spinIntegral->apply();

    ui->spinDerivative->setValue(init->kd());
    ui->spinDerivative->apply();

    ui->spinPresetT->setValue(init->presett());
    ui->spinPresetT->apply();

    ui->spinPwmMax->setValue(init->pwmmax());
    ui->spinPwmMax->apply();

    ui->spinPwmPreset->setValue(init->pwmout());
    ui->spinPwmPreset->apply();

    ui->cmbMode->setCurrentIndex(init->state());
    m_bNeedChangeMode = false;
    blockSignals(false);
}

void GuiZoneSettings::setInitialParameters(Thermo::HostToDevice *init)
{
    blockSignals(true);
    ui->spinProportional->setValue(init->kp());
    ui->spinProportional->apply();

    ui->spinIntegral->setValue(init->ki());
    ui->spinIntegral->apply();

    ui->spinDerivative->setValue(init->kd());
    ui->spinDerivative->apply();

    ui->spinPresetT->setValue(init->presett());
    ui->spinPresetT->apply();

    ui->spinPwmMax->setValue(init->pwmmaximum());
    ui->spinPwmMax->apply();

    ui->spinPwmPreset->setValue(init->pwmout());
    ui->spinPwmPreset->apply();

    ui->cmbMode->setCurrentIndexProto(init->mode());
    m_bNeedChangeMode = false;
    blockSignals(false);
}

void GuiZoneSettings::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape) {
        close();
    } else {
        QWidget::keyPressEvent(e);
    }
}

void GuiZoneSettings::myAccepted()
{
    qDebug() << "myAccepted in Dialog";
    m_bChanged = true;
    emit accepted();
}

void GuiZoneSettings::discard()
{
    ui->chkResetPid->setChecked(false);
}

void GuiZoneSettings::slotModeChanged(int index)
{
Q_UNUSED(index);

    m_bNeedChangeMode = true;
    m_bChanged = true;
    emit accepted();
}

void GuiZoneSettings::slotResetPidChanged(int state)
{
    m_bChanged = (state == Qt::Checked) ? true : false;
}


void GuiZoneSettings::start(bool started)
{
    ui->plotTemperature->start(started);
}

void GuiZoneSettings::slotUpdateFields()
{
//float CurrentT = DataDriver::getInstance().getCurrentT(m_iNumber);
//    ui->editCurrentT->setText(tr("%1").arg(CurrentT, 0, 'f', 1));
//    ui->editPWMCurrent->setText(tr("%1").arg(DataDriver::getInstance().getPwmOut(m_iNumber)));
    //    ui->plotTemperature->appendPoint0(CurrentT);
}

void GuiZoneSettings::setPresetT(int number, double T)
{
    if (number == this->m_iNumber) {
        ui->editPresetTRo->setText(QString::number(T, 'f', 1));
    }
}

void GuiZoneSettings::setCurrentT(int number, double T)
{
    if (number == this->m_iNumber) {
        ui->editCurrentT->setText(QString::number(T, 'f', 1));
        ui->plotTemperature->appendPoint0(T);
    }
}

void GuiZoneSettings::setPwmCurrent(int number, quint32 pwm)
{
    if (number == this->m_iNumber)
        ui->editPWMCurrent->setText(QString::number(pwm));
}
