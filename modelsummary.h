#ifndef MODELSUMMARY_H
#define MODELSUMMARY_H

#include <QStringList>
#include <QAbstractTableModel>
#include <QMap>

#include "VGFThermo.pb.h"

class ModelSummary : public QAbstractTableModel
{
    Q_OBJECT

public:
    ModelSummary(QObject *parent);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    void setInitialParameters(Thermo::HostToDevice *init);

private slots:
    void updatePresetT(int zoneNumber, double T);
    void updateCurrentT(int zoneNumber, double T);
    void updatePwmOut(int zoneNumber, quint32 pwm);
    void updateSendiag(int zoneNumber, quint32 flag);
    void updateRegulatorMode(int zoneNumber, Thermo::RegulatorMode mode);


private:
    QStringList m_lHeaders;

    QMap<quint32, float> m_mCurrentT;
    QMap<quint32, float> m_mPresetT;

    QMap<quint32, quint32> m_mPwmOut;

    QMap<quint32, float> m_mKp;
    QMap<quint32, float> m_mKi;
    QMap<quint32, float> m_mKd;

    QMap<quint32, Thermo::RegulatorMode> m_mRegMode;
};

#endif // MODELSUMMARY_H
