#ifndef GUIZONESETTINGS_H
#define GUIZONESETTINGS_H

#include <QWidget>
#include <QDebug>
#include "intspinboxproto.h"
#include "doublespinboxproto.h"
#include "protocol_tcp.pb.h"
#include "VGFThermo.pb.h"


namespace Ui {
class GuiZoneSettings;
}

class GuiZoneSettings : public QWidget
{
    Q_OBJECT

public:
    explicit GuiZoneSettings(int number, QWidget *parent = 0);
    ~GuiZoneSettings();

    bool getHostToDevice(Thermo::Payload *msg);
    void setInitialParameters(TCP::HostToDevice *init);
    void setInitialParameters(Thermo::HostToDevice *init);

    void keyPressEvent(QKeyEvent *e);

private slots:
    void myAccepted();
    void discard();

    void slotModeChanged(int index);
    void slotResetPidChanged(int state);

    void start(bool started);

signals:
    void accepted();
    void allClose();

public slots:
    void slotUpdateFields();

    void setPresetT(int number, double T);
    void setCurrentT(int number, double T);
    void setPwmCurrent(int number, quint32 pwm);

private:
    Ui::GuiZoneSettings *ui;

    int m_iNumber;
    bool m_bChanged;
    bool m_bNeedChangeMode;

    QList<IntSpinBoxProto *>    m_lIntSpinBoxes;
    QList<DoubleSpinBoxProto *> m_lDoubleSpinBoxes;
};

#endif // GUIZONESETTINGS_H
