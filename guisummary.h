#ifndef GUISUMMARY_H
#define GUISUMMARY_H

#include <QWidget>
#include <QPushButton>
#include <QKeyEvent>

#include "modelsummary.h"

namespace Ui {
class GuiSummary;
}

class GuiSummary : public QWidget
{
    Q_OBJECT

public:
    explicit GuiSummary(ModelSummary *model, QWidget *parent = 0);
    ~GuiSummary();

    void keyPressEvent(QKeyEvent *e);

public slots:
    void updateAll();

private:
    Ui::GuiSummary *ui;

    ModelSummary *m_pModel;
};

#endif // GUISUMMARY_H
