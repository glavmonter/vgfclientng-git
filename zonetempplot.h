#include <qwt_plot.h>
#include <qwt_interval.h>
#include <qwt_system_clock.h>
#include <QFile>


class QwtPlotCurve;
class QwtPlotMarker;
class QwtPlotDirectPainter;

class ZoneTempPlot: public QwtPlot
{
    Q_OBJECT

public:
    ZoneTempPlot(QWidget * = NULL);
    virtual ~ZoneTempPlot();

    virtual void replot();
    virtual void timerEvent(QTimerEvent *);

public Q_SLOTS:
    void setIntervalLength(double);
    void appendPoint0(double u);
    void appendCurrentT(double ts, double T);
    void appendPresetT(double ts, double T);

    void start(bool started);

protected:
    virtual void resizeEvent(QResizeEvent *);

private:
    void initGradient();
    void updateCurve();

    void incrementInterval();

    QwtPlotCurve *curveCurrentT;
    int paintedPointsCurrentT;

    QwtPlotDirectPainter *painterCurrentT;

    QwtInterval d_interval;
    int d_timerId;

    QwtSystemClock d_clock;
};
