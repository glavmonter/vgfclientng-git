#ifndef FRAMEZONE_H
#define FRAMEZONE_H

#include <QFrame>
#include <QMenu>
#include <QDebug>
#include "VGFThermo.pb.h"

namespace Ui {
class FrameZone;
}

#define ST_SHORT_THLD_FLAG(st) (st & 0x80)
#define ST_RAILS_FLAG(st)      (st & 0x40)
#define ST_POR_AFT_LST_RD(st)  (st & 0x20)
#define ST_OFLO_FLAGS(st)      ((st & 0x18) >> 3)


class FrameZone : public QFrame
{
    Q_OBJECT
    
public:
    FrameZone(int number, QWidget *parent = 0);
    ~FrameZone();

signals:
    void showSettings();

public slots:
    void UpdateRoFields();
    void setCurrentT(int zoneIndex, double T);
    void setSendiag(int zoneIndex, quint32 sendiag);
    void setRegulatorMode(int zoneIndex, Thermo::RegulatorMode mode);

private slots:
    void on_ledStatus_customContextMenuRequested(const QPoint &pos);
    void ContextMenuRequested(const QPoint &pos);

private:
    Ui::FrameZone *ui;
    int m_iNumber;

    Thermo::DeviceToHost_SendiagFlags m_xSendiag;
    Thermo::RegulatorMode m_xRegulatorMode;

    void updateColor();
};

#endif // FRAMEZONE_H
