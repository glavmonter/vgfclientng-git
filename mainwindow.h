#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDebug>
#include <QTimer>
#include <QTcpSocket>
#include <QSettings>

#include "framezone.h"
#include "guizonesettings.h"
#include "guigroupsettings.h"
#include "guisummary.h"
#include "modelsummary.h"
#include "thermocoupleconverter.h"

#include "protocol_tcp.pb.h"
#include "VGFThermo.pb.h"
#include "VGFPlatinum.pb.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    ModelSummary *m_pModelSummary;

private slots:
    void dialogAccepted();
    void groupAccepted();

    void slotTcpError(QAbstractSocket::SocketError);
    void slotTcpReadyRead();

    void on_btnConnect_clicked();
    void on_btnDisconnect_clicked();

    void slotGetData();

    void on_btnInitial_clicked();

signals:
    void setStarted(bool started);

    void updatePresetT(int zoneNumber, double T);
    void updateCurrentT(int zoneNumber, double T);
    void updatePwmOut(int zoneNumber, quint32 pwm);
    void updateStatus(int zoneNumber, quint32 status);
    void updateSendiag(int zoneNumber, quint32 flag);
    void updateRegulatorMode(int zoneNumber, Thermo::RegulatorMode mode);

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::MainWindow *ui;
    QTcpSocket *m_pTcpSocket;

    QList<FrameZone *> m_lFrameZones;
    QList<GuiZoneSettings *> m_lZoneSettings;
    GuiGroupSettings *m_pGroupSettings;
    GuiSummary *m_pGuiSummary;

    QTimer *m_pTimer1s;
    bool isStarted;
    bool isConnected;

    double m_dTCold = 0.0;
};

#endif // MAINWINDOW_H
