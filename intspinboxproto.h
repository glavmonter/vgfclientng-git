#ifndef INTSPINBOXPROTO_H
#define INTSPINBOXPROTO_H

#include <QObject>
#include <QSpinBox>
#include <QKeyEvent>
#include <QDebug>
#include <google/protobuf/message.h>

class IntSpinBoxProto : public QSpinBox
{
    Q_OBJECT
    Q_PROPERTY(QString fieldName READ fieldName WRITE setFieldName)

public:
    explicit IntSpinBoxProto(QWidget *parent = 0);
    ~IntSpinBoxProto();

private:
    void keyPressEvent(QKeyEvent *event);
    void setChanged(bool ch);

    int m_iLastData;
    bool m_bChanged;

    QString m_sFieldName;

private slots:
    void onValueChanged(int val);

public slots:
    void apply();
    void discard();

public:
    bool getData(google::protobuf::Message *msg);

    QString fieldName() const;
    void setFieldName(const QString &str);
};

#endif // INTSPINBOXPROTO_H
