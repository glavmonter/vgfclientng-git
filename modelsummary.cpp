#include "modelsummary.h"


ModelSummary::ModelSummary(QObject *parent)
    : QAbstractTableModel(parent)
{
    m_lHeaders  << tr("Zones")
                << tr("Tc") << tr("Tp") << tr("Error")
                << tr("PWM") << tr("Power")
                << tr("P") << tr("I") << tr("D")
                << tr("Mode");

    for (quint32 i = 0; i < 24; i++) {
        m_mCurrentT[i] = (i+1)*24.0f;
        m_mPresetT[i] = (i+1)*25.0f;
        m_mPwmOut[i] = (i+1)*80;

        m_mKp[i] = 12.0f;
        m_mKi[i] = 36.5f;
        m_mKd[i] = 94.6f;

        m_mRegMode[i] = Thermo::MODE_STOPPED;
    }
}


int ModelSummary::rowCount(const QModelIndex &parent) const
{
Q_UNUSED(parent);
    return 22;
}

int ModelSummary::columnCount(const QModelIndex &parent) const
{
Q_UNUSED(parent);
    return m_lHeaders.count();
}

QVariant ModelSummary::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole) {
        int ZoneNumber = index.row();
        switch (index.column()) {
        case 0:
            return tr("Zone %1").arg(ZoneNumber + 1);
            break;

        case 1:
            return QString::number(m_mCurrentT[ZoneNumber], 'f', 1) + " °C";
            break;

        case 2:
            return QString::number(m_mPresetT[ZoneNumber], 'f', 1) + " °C";
            break;

        case 3:
            return QString::number(m_mPresetT[ZoneNumber] - m_mCurrentT[ZoneNumber], 'f', 1) + " °C";
            break;

        case 4:
            return QString::number(m_mPwmOut[ZoneNumber]);
            break;

        case 5:
            return QString::number(m_mPwmOut[ZoneNumber]*100.0f/65535.0f, 'f', 1) + "%";
            break;

        case 6:
            return QString::number(m_mKp[ZoneNumber], 'f', 3);
            break;

        case 7:
            return QString::number(m_mKi[ZoneNumber], 'f', 3);
            break;

        case 8:
            return QString::number(m_mKd[ZoneNumber], 'f', 3);
            break;

        case 9:
            switch (m_mRegMode[ZoneNumber]) {
            case Thermo::MODE_ACCIDENT:
                return tr("Accident");
                break;

            case Thermo::MODE_MANUAL:
                return tr("Manual");
                break;

            case Thermo::MODE_PID:
                return tr("PID");
                break;

            case Thermo::MODE_RELAY:
                return tr("Relay");
                break;

            case Thermo::MODE_STOPPED:
                return tr("Stopped");
                break;
            }

            break;
        default:
            break;
        }
    }
    return QVariant();
}

QVariant ModelSummary::headerData(int section, Qt::Orientation orientation, int role) const
{
    if ((role == Qt::DisplayRole) && (orientation == Qt::Horizontal))
        return m_lHeaders.at(section);
    return QVariant();
}

void ModelSummary::setInitialParameters(Thermo::HostToDevice *init)
{
}

void ModelSummary::updatePresetT(int zoneNumber, double T)
{
    m_mPresetT[zoneNumber] = T;
}

void ModelSummary::updateCurrentT(int zoneNumber, double T)
{
    m_mCurrentT[zoneNumber] = T;
}

void ModelSummary::updatePwmOut(int zoneNumber, quint32 pwm)
{
    m_mPwmOut[zoneNumber] = pwm;
}

void ModelSummary::updateSendiag(int zoneNumber, quint32 flag)
{
}

void ModelSummary::updateRegulatorMode(int zoneNumber, Thermo::RegulatorMode mode)
{
    m_mRegMode[zoneNumber] = mode;
}

