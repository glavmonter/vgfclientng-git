<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>DoubleSpinBoxProto</name>
    <message>
        <location filename="../doublespinboxproto.cpp" line="29"/>
        <source>%1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FrameZone</name>
    <message>
        <location filename="../framezone.ui" line="41"/>
        <source>Current temperature</source>
        <translation>Текущая температура</translation>
    </message>
    <message>
        <location filename="../framezone.ui" line="136"/>
        <source>Led widget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../framezone.ui" line="164"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../framezone.ui" line="174"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../framezone.ui" line="177"/>
        <source>Show zone settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../framezone.cpp" line="11"/>
        <source>%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../framezone.cpp" line="36"/>
        <source>Settings %1</source>
        <translation>Настройки %1</translation>
    </message>
    <message>
        <location filename="../framezone.cpp" line="54"/>
        <source>Stopped. Closed thermocouple</source>
        <translation>Остановлено. Нормальная термопара</translation>
    </message>
    <message>
        <location filename="../framezone.cpp" line="60"/>
        <source>Stopped. Open thermocouple</source>
        <translation>Остановлено. Обрыв термопары</translation>
    </message>
    <message>
        <location filename="../framezone.cpp" line="66"/>
        <source>Running. Closed thermocouple</source>
        <translation>Работа. Нормальная термопара</translation>
    </message>
    <message>
        <location filename="../framezone.cpp" line="72"/>
        <source>Running. Open thermocouple</source>
        <translation>Работа. Обрыв термопары</translation>
    </message>
</context>
<context>
    <name>GuiGroupSettings</name>
    <message>
        <location filename="../guigroupsettings.ui" line="14"/>
        <source>Group zones settings</source>
        <translation>Групповое редактирование зон</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="24"/>
        <source>Parameters</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="38"/>
        <source>Stop</source>
        <translation>Останов</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="43"/>
        <source>Manual</source>
        <translation>Ручной</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="48"/>
        <source>Relay</source>
        <translation>Релейный</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="53"/>
        <source>PID</source>
        <translation>ПИД</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="61"/>
        <source>Reset PID</source>
        <translation>Сброс ПИД</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="71"/>
        <source>Integral:</source>
        <translation>Интегральный:</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="81"/>
        <source>PWM Out:</source>
        <translation>ШИМ выход:</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="91"/>
        <source>PWM Max:</source>
        <translation>ШИМ максимум:</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="101"/>
        <source>Proportional:</source>
        <translation>Пропорциональный:</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="114"/>
        <source>Derivative:</source>
        <translation>Дифференциальный:</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="127"/>
        <source>Mode:</source>
        <translation>Режим:</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="137"/>
        <source>Preset T:</source>
        <translation>Уставка, °C:</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.ui" line="192"/>
        <source>Zones</source>
        <translation>Зоны</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.cpp" line="28"/>
        <source>1-8</source>
        <translation>1-8</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.cpp" line="29"/>
        <source>9-16</source>
        <translation>9-16</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.cpp" line="30"/>
        <source>17-24</source>
        <translation>17-24</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.cpp" line="32"/>
        <source>All</source>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="../guigroupsettings.cpp" line="45"/>
        <location filename="../guigroupsettings.cpp" line="54"/>
        <location filename="../guigroupsettings.cpp" line="63"/>
        <source>Zone %1</source>
        <translation>Зона %1</translation>
    </message>
</context>
<context>
    <name>GuiSummary</name>
    <message>
        <location filename="../guisummary.ui" line="14"/>
        <source>Summary</source>
        <translation>Сводка по зонам</translation>
    </message>
</context>
<context>
    <name>GuiZoneSettings</name>
    <message>
        <location filename="../guizonesettings.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="43"/>
        <source>Integral:</source>
        <translation>Интегральный:</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="53"/>
        <source>Proportional:</source>
        <translation>Пропорциональный:</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="63"/>
        <source>Derivative:</source>
        <translation>Дифференциальный:</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="73"/>
        <location filename="../guizonesettings.ui" line="218"/>
        <location filename="../guizonesettings.ui" line="408"/>
        <source>Lock</source>
        <translation>Заблокировать</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="86"/>
        <source>Reset PID</source>
        <translation>Сброс ПИД</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="93"/>
        <source>Gain:</source>
        <translation>Усиление:</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="198"/>
        <source>Current T:</source>
        <translation>Текущая. °C:</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="208"/>
        <source>PWM max:</source>
        <translation>ШИМ максимум:</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="231"/>
        <location filename="../guizonesettings.ui" line="421"/>
        <source>0-65535</source>
        <translation>0-65535</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="254"/>
        <location filename="../guizonesettings.ui" line="299"/>
        <source>Preset T:</source>
        <translation>Уставка, °C:</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="270"/>
        <source> C</source>
        <translation> °C</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="341"/>
        <source>Mode:</source>
        <translation>Режим:</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="355"/>
        <source>Stop</source>
        <translation>Останов</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="360"/>
        <source>Manual</source>
        <translation>Ручной</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="365"/>
        <source>Relay</source>
        <translation>Релейный</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="370"/>
        <source>PID</source>
        <translation>ПИД</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="378"/>
        <source>PWM Current:</source>
        <translation>ШИМ выход:</translation>
    </message>
    <message>
        <location filename="../guizonesettings.ui" line="398"/>
        <source>PWM Preset:</source>
        <translation>ШИМ уставка:</translation>
    </message>
    <message>
        <location filename="../guizonesettings.cpp" line="15"/>
        <source>Zone %1</source>
        <translation>Зона %1</translation>
    </message>
    <message>
        <location filename="../guizonesettings.cpp" line="74"/>
        <source>HostToDevice: %1 zone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../guizonesettings.cpp" line="94"/>
        <source>Need change Mode</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IntSpinBoxProto</name>
    <message>
        <location filename="../intspinboxproto.cpp" line="30"/>
        <source>Last value %1</source>
        <translation>Прошлое значение %1</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>VGFClient next generation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="37"/>
        <source>Zones</source>
        <translation>Зоны</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="67"/>
        <source>Disconnect</source>
        <translation>Отключить</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="74"/>
        <location filename="../mainwindow.ui" line="213"/>
        <source>Connect</source>
        <translation>Подключить</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="101"/>
        <source>Get Data</source>
        <translation>Получить данные</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="108"/>
        <source>Set Initial</source>
        <translation>Начальные настройки</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="115"/>
        <source>Summary</source>
        <translation>Сводка</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="122"/>
        <source>Edit &amp;All</source>
        <translation>Г&amp;рупповое</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="148"/>
        <source>Appl&amp;y</source>
        <translation>При&amp;менить</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="158"/>
        <source>Ca&amp;ncel</source>
        <translation>Отм&amp;енить</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="181"/>
        <source>E&amp;xit</source>
        <translation>В&amp;ыход</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="203"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="218"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="225"/>
        <source>Sync number: %1</source>
        <translation>Номер отсчета %1</translation>
    </message>
</context>
<context>
    <name>ModelSummary</name>
    <message>
        <location filename="../modelsummary.cpp" line="7"/>
        <source>Zones</source>
        <translation>Зоны</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="8"/>
        <source>Tc</source>
        <translation>Тт</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="8"/>
        <source>Tp</source>
        <translation>Ту</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="8"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="9"/>
        <source>PWM</source>
        <translation>ШИМ</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="9"/>
        <source>Power</source>
        <translation>Мощность</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="10"/>
        <source>P</source>
        <translation>П</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="10"/>
        <source>I</source>
        <translation>И</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="10"/>
        <source>D</source>
        <translation>Д</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="11"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="45"/>
        <source>Zone %1</source>
        <translation>Зона %1</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="83"/>
        <source>Accident</source>
        <translation>Авария</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="87"/>
        <source>Manual</source>
        <translation>Ручное</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="91"/>
        <source>PID</source>
        <translation>ПИД</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="95"/>
        <source>Relay</source>
        <translation>Релейный</translation>
    </message>
    <message>
        <location filename="../modelsummary.cpp" line="99"/>
        <source>Stopped</source>
        <translation>Останов</translation>
    </message>
</context>
<context>
    <name>PlotFurnace</name>
    <message>
        <location filename="../plotfurnace.cpp" line="54"/>
        <source>Zone</source>
        <translation>Зона</translation>
    </message>
    <message>
        <location filename="../plotfurnace.cpp" line="56"/>
        <source>Temperature [°C]</source>
        <translation>Температура [°C]</translation>
    </message>
    <message>
        <location filename="../plotfurnace.cpp" line="88"/>
        <source>Preset</source>
        <translation>Уставка</translation>
    </message>
    <message>
        <location filename="../plotfurnace.cpp" line="99"/>
        <source>Current</source>
        <translation>Текущая</translation>
    </message>
</context>
</TS>
