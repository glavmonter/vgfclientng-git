#include "comboboxproto.h"

ComboBoxProto::ComboBoxProto(QWidget *parent) :
    QComboBox(parent), m_xType(ComboBoxProtoType::TYPE_NORMAL)
{
    setType(m_xType);
}

ComboBoxProto::~ComboBoxProto()
{
}

void ComboBoxProto::setType(ComboBoxProtoType type)
{
    m_xType = type;
    switch (m_xType) {
    case ComboBoxProtoType::TYPE_NORMAL:
        m_mIndexToMode[0] = Thermo::MODE_STOPPED;
        m_mIndexToMode[1] = Thermo::MODE_MANUAL;
        m_mIndexToMode[2] = Thermo::MODE_RELAY;
        m_mIndexToMode[3] = Thermo::MODE_PID;

        m_mModeToIndex[Thermo::MODE_STOPPED] = 0;
        m_mModeToIndex[Thermo::MODE_MANUAL] = 1;
        m_mModeToIndex[Thermo::MODE_RELAY] = 2;
        m_mModeToIndex[Thermo::MODE_PID] = 3;
        break;

    case ComboBoxProtoType::TYPE_GROUP:
        m_mIndexToMode[1] = Thermo::MODE_STOPPED;
        m_mIndexToMode[2] = Thermo::MODE_MANUAL;
        m_mIndexToMode[3] = Thermo::MODE_RELAY;
        m_mIndexToMode[4] = Thermo::MODE_PID;

        m_mModeToIndex[Thermo::MODE_STOPPED] = 1;
        m_mModeToIndex[Thermo::MODE_MANUAL] = 2;
        m_mModeToIndex[Thermo::MODE_RELAY] = 3;
        m_mModeToIndex[Thermo::MODE_PID] = 4;
        break;

    default:
        break;
    }
}

Thermo::RegulatorMode ComboBoxProto::getMode()
{
    return m_mIndexToMode[currentIndex()];
}

void ComboBoxProto::setCurrentIndexProto(Thermo::RegulatorMode mode)
{
    setCurrentIndex(m_mModeToIndex[mode]);
}

