#include <iostream>

#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), isStarted(false), isConnected(false)
{
    ui->setupUi(this);

    m_pTcpSocket = new QTcpSocket();

    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotTcpError(QAbstractSocket::SocketError)));
    connect(m_pTcpSocket, &QTcpSocket::readyRead, this, &MainWindow::slotTcpReadyRead);

    m_pGroupSettings = new GuiGroupSettings();
    connect(m_pGroupSettings, &GuiGroupSettings::accepted, this, &MainWindow::groupAccepted);

    m_pModelSummary = new ModelSummary(this);
    m_pGuiSummary = new GuiSummary(m_pModelSummary);
    connect(ui->btnSummary, &QPushButton::clicked, m_pGuiSummary, &GuiSummary::raise);
    connect(ui->btnSummary, &QPushButton::clicked, m_pGuiSummary, &GuiSummary::show);

    connect(this, SIGNAL(updateCurrentT(int,double)), m_pModelSummary, SLOT(updateCurrentT(int,double)));
    connect(this, SIGNAL(updatePresetT(int,double)), m_pModelSummary, SLOT(updatePresetT(int,double)));
    connect(this, SIGNAL(updatePwmOut(int,quint32)), m_pModelSummary, SLOT(updatePwmOut(int,quint32)));
    connect(this, SIGNAL(updateRegulatorMode(int,Thermo::RegulatorMode)), m_pModelSummary, SLOT(updateRegulatorMode(int,Thermo::RegulatorMode)));

    /* Строим интерфейс пользователя */
    QHBoxLayout *hBoxZones = new QHBoxLayout(ui->groupZones);

    FrameZone *z;
    GuiZoneSettings *zs;
    ui->groupZones->setEnabled(false);
    connect(this, SIGNAL(updateCurrentT(int,double)), ui->frame, SLOT(setCurrentT(int,double)));
    connect(this, SIGNAL(updatePresetT(int,double)), ui->frame, SLOT(setPresetT(int,double)));
    connect(ui->btnEditAll, &QPushButton::clicked, m_pGroupSettings, &GuiGroupSettings::raise);
    connect(ui->btnEditAll, &QPushButton::clicked, m_pGroupSettings, &GuiGroupSettings::show);

    for (int i = 0; i < 24; i++) {
        z = new FrameZone(i, ui->groupZones);
        connect(this, SIGNAL(updateCurrentT(int,double)), z, SLOT(setCurrentT(int,double)));
        connect(this, SIGNAL(updateSendiag(int,quint32)), z, SLOT(setSendiag(int,quint32)));
        connect(this, SIGNAL(updateRegulatorMode(int,Thermo::RegulatorMode)), z, SLOT(setRegulatorMode(int,Thermo::RegulatorMode)));

        zs = new GuiZoneSettings(i, NULL);
        connect(this, SIGNAL(setStarted(bool)), zs, SLOT(start(bool)));

        connect(this, SIGNAL(updateCurrentT(int,double)), zs, SLOT(setCurrentT(int,double)));
        connect(this, SIGNAL(updatePresetT(int,double)), zs, SLOT(setPresetT(int,double)));
        connect(this, SIGNAL(updatePwmOut(int,quint32)), zs, SLOT(setPwmCurrent(int,quint32)));

        m_lFrameZones << z;
        m_lZoneSettings << zs;
        connect(z, &FrameZone::showSettings, zs, &GuiZoneSettings::raise);
        connect(z, &FrameZone::showSettings, zs, &GuiZoneSettings::show);
        connect(zs, &GuiZoneSettings::accepted, this, &MainWindow::dialogAccepted);

        hBoxZones->addWidget(z);
    }
    m_lFrameZones[23]->setEnabled(false);
    m_lFrameZones[22]->setEnabled(false);
    m_lFrameZones[23]->hide();
    m_lFrameZones[22]->hide();

    connect(ui->btnExit, SIGNAL(clicked()), this, SLOT(close()));

    m_pTimer1s = new QTimer(this);
    connect(m_pTimer1s, &QTimer::timeout, this, &MainWindow::slotGetData);
    connect(m_pTimer1s, &QTimer::timeout, m_pGuiSummary, &GuiSummary::updateAll);

QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
QRegExp ipRegex("^" + ipRange
                + "\\." + ipRange
                + "\\." + ipRange
                + "\\." + ipRange + "$");

    ui->editIP->setValidator(new QRegExpValidator(ipRegex, this));

QSettings settings;
    ui->editIP->setText(settings.value("address", "127.0.0.1").toString());

    connect(ui->btnGetData, SIGNAL(clicked()), this, SLOT(slotGetData()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::slotTcpError(QAbstractSocket::SocketError)
{
    m_pTimer1s->stop();

    QMessageBox::critical(this, "Error", m_pTcpSocket->errorString());
    ui->btnConnect->setEnabled(true);
    ui->btnDisconnect->setEnabled(false);
    ui->editIP->setEnabled(true);
    ui->groupZones->setEnabled(false);
}


void MainWindow::dialogAccepted()
{
    qDebug() << QString("Accepted in MainWindow");

TCP::ClientServer cts;
bool real_diff = false;

Thermo::Payload payload;

    payload.set_deviceid(1);

    foreach (GuiZoneSettings *zs, m_lZoneSettings) {
        if (zs->getHostToDevice(&payload))
            real_diff = true;
    }

    if (real_diff) {
        std::cout << "All diff completed" << std::endl;
        std::cout << payload.DebugString() << std::endl;
        std::cout << "TODO Serialize and send to server" << std::endl;

        cts.set_type(TCP::ClientServer_ClientType_CLIENT_LOCAL);
        cts.set_setdata(TCP::SET_CLIENT_SETTINGS);
        cts.set_thermo(payload.SerializeAsString());

        std::string str;
        cts.SerializeToString(&str);
        m_pTcpSocket->write(str.data(), str.size());
    }
}

void MainWindow::groupAccepted()
{
TCP::ClientServer cts;
Thermo::Payload payload;
    payload.set_deviceid(1);

    if (m_pGroupSettings->getHostToDevice(&payload)) {
        cts.set_type(TCP::ClientServer_ClientType_CLIENT_LOCAL);
        cts.set_setdata(TCP::SET_CLIENT_SETTINGS);

        std::cout << "Group accepted" << std::endl;
        std::cout << cts.DebugString() << std::endl;
        std::cout << "Serialize and send to server" << std::endl;
        cts.set_thermo(payload.SerializeAsString());

        std::string str;
        cts.SerializeToString(&str);
        m_pTcpSocket->write(str.data(), str.size());
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    foreach (GuiZoneSettings *zs, m_lZoneSettings) {
        delete zs;
    }

    delete m_pGroupSettings;
    delete m_pGuiSummary;
    event->accept();
}


void MainWindow::on_btnConnect_clicked()
{
    ui->btnConnect->setEnabled(false);
    ui->editIP->setEnabled(false);

    m_pTcpSocket->connectToHost(ui->editIP->text(), 9999);

    if (m_pTcpSocket->waitForConnected(5000) == false) {
        ui->btnConnect->setEnabled(true);
        ui->btnDisconnect->setEnabled(false);
        ui->groupZones->setEnabled(false);
        ui->editIP->setEnabled(true);
        return;
    }

QSettings settings;
    settings.setValue("address", ui->editIP->text());

TCP::ClientServer cts;
    cts.set_type(TCP::ClientServer_ClientType_CLIENT_LOCAL);
    std::string str = cts.SerializePartialAsString();
    m_pTcpSocket->write(str.data(), str.size());

    ui->btnDisconnect->setEnabled(true);
    ui->groupZones->setEnabled(true);

    isStarted = true;

    emit setStarted(true);
    m_pTimer1s->start(1000);
}

void MainWindow::on_btnDisconnect_clicked()
{
    m_pTimer1s->stop();

    isStarted = false;
    emit setStarted(false);

    m_pTcpSocket->disconnectFromHost();
    ui->editIP->setEnabled(true);
    ui->btnConnect->setEnabled(true);
    ui->btnDisconnect->setEnabled(false);
    ui->groupZones->setEnabled(false);
}

void MainWindow::slotGetData()
{
TCP::ClientServer cts;
    cts.set_type(TCP::ClientServer_ClientType_CLIENT_LOCAL);
    cts.set_getdata(TCP::GET_CLIENT_STATE);

    std::string str = cts.SerializeAsString();
    m_pTcpSocket->write(str.data(), str.size());
}

void MainWindow::slotTcpReadyRead()
{
QByteArray tcpData = m_pTcpSocket->readAll();
TCP::ClientStateReply reply;
    reply.ParseFromArray(tcpData.data(), tcpData.size());

    if (reply.getdata() == TCP::GET_CLIENT_STATE) {
        if (reply.has_syncnumber())
            ui->statusBar->showMessage(tr("Sync number: %1").arg(reply.syncnumber()));

        if (reply.has_thermos()) {
            Thermo::Payload pp;
            pp.ParseFromString(reply.thermos());

            for (int z = 0; z < pp.dth_size(); z++) {
                int zone = pp.mutable_dth(z)->zonenumber();

                emit updateCurrentT(zone, pp.mutable_dth(z)->currentt());
                emit updatePresetT(zone, pp.mutable_dth(z)->presett());
                emit updatePwmOut(zone, pp.mutable_dth(z)->pwmout());
                emit updateSendiag(zone, pp.mutable_dth(z)->sendiag());
                emit updateRegulatorMode(zone, pp.mutable_dth(z)->mode());
            }
        }

        if (reply.has_platinum()) {
            Platinum::Payload pp;
            pp.ParseFromString(reply.platinum());
            ThermocoupleTypeS S;
            double ColdEmfS = S.GetEmf(m_dTCold)*1000.0;

            ui->labelPt1->setText(tr("Pt(7) %1 °C")
                                  .arg(QString::number(S.GetTemperature(pp.dth().temperaturept(0) + ColdEmfS), 'f', 2)));
            ui->labelPt2->setText(tr("Pt(9) %1 °C")
                                  .arg(QString::number(S.GetTemperature(pp.dth().temperaturept(1) + ColdEmfS), 'f', 2)));
            ui->labelPt3->setText(tr("Pt(19) %1 °C")
                                  .arg(QString::number(S.GetTemperature(pp.dth().temperaturept(2) + ColdEmfS), 'f', 2)));

            double cold = 0.0;
            if (pp.dth().temperatureds_size() > 0) {
                int colds = 0;
                for (int i = 4; i < pp.dth().temperatureds_size() - 1; i++) {
                    double T = pp.dth().temperatureds(i);
                    if (T > 0.3) {
                        cold += T;
                        colds += 1;
                    }
                }
                cold /= colds;
            }
            if (cold > 0.3) {
                m_dTCold = cold;
                ui->labelCold->setText(tr("Cold: %1 °C").arg(QString::number(m_dTCold, 'f', 2)));
            }

        }
    } else if (reply.getdata() == TCP::GET_CLIENT_SETTINGS) {
        qDebug() << "Тут должны быть настройки";
        Thermo::Payload pp;
        pp.ParseFromString(reply.thermos());
        std::cout << pp.DebugString() << std::endl;

        for (int z = 0; z < pp.htd_size(); z++) {
            m_lZoneSettings[z]->setInitialParameters(pp.mutable_htd(z));
        }
    }
}

void MainWindow::on_btnInitial_clicked()
{
TCP::ClientServer cts;
std::string str;
    cts.set_type(TCP::ClientServer_ClientType_CLIENT_LOCAL);
    cts.set_getdata(TCP::GET_CLIENT_SETTINGS);
    cts.SerializePartialToString(&str);
    m_pTcpSocket->write(str.data(), str.size());
}
